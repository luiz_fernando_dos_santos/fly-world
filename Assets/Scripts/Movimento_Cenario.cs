﻿using UnityEngine;
using System.Collections;

public class Movimento_Cenario : MonoBehaviour {

	public GameManager gameManager;

	public GameObject nuvem1;
	public GameObject nuvem2;

	public float distancia_Nascimento;

	// Use this for initialization
	void Start () {
 		
	}

	// Update is called once per frame
	void Update () {
		Vector3 newPosition = transform.position;
		newPosition.z +=  (-(Time.deltaTime*(gameManager.velocidade_Cenario)));
		transform.position = newPosition;


	}

	void OnTriggerEnter(Collider outro)
	{
		if(outro.tag=="cria_objeto")
		{
			
			Instantiate (nuvem1, new Vector3 (transform.position.x, transform.position.y, transform.position.z + distancia_Nascimento), Quaternion.identity);

		}
		if (outro.tag == "destroi_objeto"){
			Destroy(gameObject);
		}

	}
}
