﻿using UnityEngine;
using System.Collections;

/*
 * Classe Pause criada para colocar a logica do botao pause do jogo
 * se nao tivesse o botao pause esta logica poderia estar no GameManager,
 * esta seria uma atualizacao a ser estudada caso o jogo necessite de mais potencia
 * 
 * */

public class Pause : MonoBehaviour {

	public static bool pausa;

	// Use this for initialization
	void Start () {

        GameManager.pausado = pausa;
	}
	
	// Update is called once per frame
	void Update () {
        
       // pausa = !GameManager.pausado;//deixa o valor de pausado diferente do valor atual
        
        if (pausa) {
            Time.timeScale = 0;

        } else if (!pausa) {
            Time.timeScale = 1;
			pausa=false;
		
        }
       
        GameManager.pausado = pausa;
    }

    public void setPause() {
        pausa = !GameManager.pausado;//deixa o valor de pausado diferente do valor atual

        if (pausa)
        {
        
            Time.timeScale = 0;
        }
        else if (!pausa)
        {
	;
            GameManager.jogador_esta_vivo = true;

            Time.timeScale = 1;
		}

        GameManager.pausado = pausa;
    }
}
