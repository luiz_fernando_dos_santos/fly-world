using UnityEngine;
using System.Collections;


/*
*	Classe responsavel por criar e movimentar 
*	objetos que fazem colisao com o personagem
*
*
**/


public class Movimenta_objetos : MonoBehaviour {

	Vector3 newPosition;
	public GameManager gameManager;

	public GameObject pedra1;
	public GameObject nuvem1;
	public GameObject nuvem2;
	public GameObject espinhos1;
	public GameObject cameraTroca;

	public int sorteio;
	public int sorteioAnterior=GameManager.sorteioAnterior;
	public static int sorteio_troca_camera;
	public static bool troca_camera;
	public float velocidade;// = GameManager.velocidade;
	public int contador = 0;
	public float distancia_Nascimento=85;

	// Use this for initialization
	void Start () {

		transform.Rotate(-90,0,0	);
	}

	// Update is called once per frame
	void Update () {

		if(Camera_Alterna.camera_Atual=="3d"){
			//GameManager.velocidade=2	;
			//velocidade = GameManager.velocidade;
		}
		if(Camera_Alterna.camera_Atual=="2d"){
			//GameManager.velocidade=2;
			//velocidade = GameManager.velocidade;
		}
		
		Vector3 newPosition = transform.position;
		newPosition.z +=  (-(Time.deltaTime*gameManager.velocidade_Objetos));
		transform.position = newPosition;


	}

	void OnTriggerEnter(Collider outro){
		sorteio = Random.Range (0, 2);
		sorteio_troca_camera = Random.Range (0, 10);
		if (sorteio_troca_camera < 7) {
			troca_camera = true;
		} else {
			//troca_camera = true;
		}
		

		if (outro.tag == "cria_objeto") {
			sorteio = Random.Range(0,2);
			//if (GameManager.sorteioAnterior==0&&sorteio==0) {
			//		sorteio = Random.Range(1,6);
					
			//}
			if(contador==0){
				
				if (sorteio == 0) {
					Instantiate (espinhos1, new Vector3 (transform.position.x, transform.position.y, transform.position.z + distancia_Nascimento), Quaternion.identity);
					GameManager.sorteioAnterior=0;
					GameManager.contador_de_vazios++;
				}
				if (sorteio == 1) {
					Instantiate (pedra1, new Vector3 ((transform.position.x+(Random.Range(-2,2))), transform.position.y, transform.position.z + distancia_Nascimento), Quaternion.identity);
					//chao_obstaculo
					GameManager.sorteioAnterior=1;
				}
				if (sorteio == 2) {
					Instantiate (pedra1, new Vector3 ((transform.position.x+(Random.Range(-2,2))), transform.position.y, transform.position.z + distancia_Nascimento), Quaternion.identity);
					GameManager.sorteioAnterior=2;
				}
				contador++;	
				
			}else if (contador>0){
				sorteioAnterior=GameManager.sorteioAnterior;
			}

			
						

		} 
		if(GameManager.contador_de_vazios==3){
			Instantiate (cameraTroca, new Vector3 (transform.position.x, transform.position.y, transform.position.z + distancia_Nascimento), Quaternion.identity);
			GameManager.contador_de_vazios=0;
		}
		if (outro.tag == "destroi_objeto"){
			Destroy(gameObject);

			if (outro.tag == "Player") {
				
				Destroy(gameObject);
			}
		}
	}
}
