﻿using UnityEngine;
using System.Collections;

public class Movimento_Offset : MonoBehaviour {

	public GameManager gameManager;

	public float scrollSpeed = 0.5F;


	public Renderer rend;
	void Start() {
		rend = GetComponent<Renderer>();
	}
	void Update() {
		float offset = Time.time * gameManager.velocidade;

		rend.material.SetTextureOffset("_MainTex", new Vector2(0, -offset));
	}
}
