﻿using UnityEngine;
using System.Collections;


/*
 *Classe GameManager - Esta e a classe para controlar o Jogo de modo geral, 
 *  	status de cenas de jogador etc.
 *
 *
 **/
public class GameManager : MonoBehaviour {

	//variaveis de status do jogo	
	public TextMesh highscores;

	public static bool jogador_esta_vivo;//verifica se jogador esta vivo para liberar a logica dos controles na classe CharacterMovement
	public bool primeiro_vez_que_joga;//variavel sera usada para controlar se vai mostrar um tutorial de coomo o jogador joga 
	public bool esta_online;//para habilitar assets online(share, banner e advideos)
    public static bool correndo = true;//para controlar status atual do jogador - talvez devesse estar na classe CharacterMovement
	public static bool pulando = false;//para controlar status atual do jogador - talvez devesse estar na classe CharacterMovement
	public static bool pausado;//para mostrar status atual do jogo

    public static int escolheViverAtivo=0;//a principio nao esta sendo usada


	//variaveis abaixo ainda nao analisada, mas podem estar sendo usadas, nao apagar
    public static int recorde;
	public int contador_1;//
	public int bonus_atual = 1;
	public int tempo_por_metro	 = 10;
	public static int contador_de_pontos;
	public static int sorteio_anterior=0;
	public static int sorteioAnterior;
	public float velocidade;
	public float velocidade_Cenario;
	public float velocidade_Objetos;

	public static bool cameraTroca=false;

	public static int contador_de_vazios;

	public float timeRemaining = 5;

	// Use this for initialization
	void Start () {

		jogador_esta_vivo = true;
        pausado = false;
		highscores.text = "Recorde:   " + PlayerPrefs.GetInt("HighScore");
	}
	
	// Update is called once per frame
	void Update () {

        if (jogador_esta_vivo && pausado == false)
        {
            //Destroy(gameObject);
      
            contador_de_pontos += (int)((Time.deltaTime / tempo_por_metro) + bonus_atual);

            GetComponent<TextMesh>().text = "Ponto:     " + contador_de_pontos;
        }
        
		if(contador_de_pontos>recorde){
			recorde=contador_de_pontos;
		}


		if (contador_de_pontos > PlayerPrefs.GetInt ("HighScore")) {
			PlayerPrefs.SetInt ("HighScore", contador_de_pontos);
		}
		highscores.text ="Recorde:   " + PlayerPrefs.GetInt("HighScore");

        if (Input.GetKeyDown(KeyCode.F1))
        {
            PlayerPrefs.SetInt("HighScore", 0);
        }

	}
}
